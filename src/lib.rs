#![no_std]
#![feature(adt_const_params)]
#![feature(generic_const_exprs)]
#![feature(array_methods)]
#![allow(incomplete_features)]

/// ```rust
/// # #![feature(variant_count)]
/// # use std::error::Error;
/// #
/// # extern crate staticbitset;
/// # fn main() -> Result<(), Box<Error>> {
/// use staticbitset::StaticBitSet;
/// #[repr(usize)]
/// enum Hats {
///     Cap,
///     Top,
///     Bowler,
///     Beret,
///     Fez,
/// }
/// use Hats::*;
/// use core::mem;
///
/// type HatBitSet = StaticBitSet::<{mem::variant_count::<Hats>()}>;
/// assert_eq!(4, mem::size_of::<HatBitSet>());
///
/// assert_eq!(8, mem::size_of::< StaticBitSet::<64> >());
/// assert_eq!(4, mem::size_of::< StaticBitSet::<32> >());
/// assert_eq!(4, mem::size_of::< StaticBitSet::<31> >());
///
/// let mut x = StaticBitSet::<64>::default();
/// assert!(!x.contains(Fez as usize));
///
/// x.insert(Fez as usize);
/// assert!(x.contains(Fez as usize));
///
/// x.clear();
/// assert!(!x.contains(Fez as usize));
///
/// x.set(Fez as usize, true);
/// assert!(x.contains(Fez as usize));
///
/// x.set(Fez as usize, false);
/// assert!(!x.contains(Fez as usize));
/// assert!(!x.contains(Bowler as usize));
///
/// for hat in x.ones().into_iter() {
///    println!("Hat index: {}", hat);
/// }
/// #     Ok(())
/// # }
/// ```

// TODO: would 'usize' be a better assumption for an optimal
// (general register-sized?) block?  Or maybe these should be
// target specific?
pub type BlockElt = u32;
pub const BLOCK_SIZE_BITS: usize = core::mem::size_of::<BlockElt>() * 8;
pub const fn bits_to_blocks<const N: usize>() -> usize {
    (N + BLOCK_SIZE_BITS - 1) / BLOCK_SIZE_BITS
}
pub type BlockElts<const N: usize> = [BlockElt; bits_to_blocks::<N>()];

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub struct StaticBitSet<const N: usize>
where
    [(); bits_to_blocks::<N>()]:,
{
    bit_blocks: BlockElts<N>,
}

impl<const N: usize> Default for StaticBitSet<N>
where
    [(); bits_to_blocks::<N>()]:,
{
    fn default() -> Self {
        Self {
            bit_blocks: [0; bits_to_blocks::<N>()],
        }
    }
}

struct BitBlockIdx {
    pub block: usize,
    pub bit: usize,
}
#[inline]
const fn index(bit: usize) -> BitBlockIdx {
    BitBlockIdx {
        block: bit / BLOCK_SIZE_BITS,
        bit: bit % BLOCK_SIZE_BITS,
    }
}

impl<const N: usize> StaticBitSet<N>
where
    [(); bits_to_blocks::<N>()]:,
{
    /// Clears the set, removing all values.
    pub fn clear(&mut self) {
        for block in self.bit_blocks.as_mut_slice() {
            *block = BlockElt::default();
        }
    }

    pub fn remove(&mut self, bit: usize) {
        self.set(bit, false);
    }

    /// Inserts `bit` into the set.
    ///
    /// # Panics
    /// Panics if 'bit' is beyond the bounds of the `StaticBitSet`.
    pub fn insert(&mut self, bit: usize) {
        self.set(bit, true);
    }

    #[inline]
    pub fn set(&mut self, bit: usize, enabled: bool) {
        let index = index(bit);
        if enabled {
            self.blocks_mut_slice()[index.block] |= 1 << index.bit;
        } else {
            self.blocks_mut_slice()[index.block] &= !(1 << index.bit);
        }
    }

    #[inline]
    pub fn contains(&self, bit: usize) -> bool {
        let index = index(bit);

        (self.blocks_slice()[index.block] & (1 << index.bit)) != 0
    }

    #[inline]
    fn blocks_slice(&self) -> &[BlockElt] {
        self.bit_blocks.as_slice()
    }

    #[inline]
    fn blocks_mut_slice(&mut self) -> &mut [BlockElt] {
        self.bit_blocks.as_mut_slice()
    }

    #[inline]
    pub fn ones(&self) -> OnesIter {
        OnesIter {
            block_index: 0,
            bit_index: 0,
            blocks: self.blocks_slice(),
        }
    }

    /// Returns the number of elements in the set.
    pub fn count_ones(&self) -> BlockElt {
        self.blocks_slice().iter().map(|x| x.count_ones()).sum()
    }

    /// Returns the number of elements missing from the set.
    pub fn count_zeros(&self) -> BlockElt {
        self.blocks_slice().iter().map(|x| x.count_zeros()).sum()
    }

    /// Returns true if the set contains no elements.
    pub fn is_empty(&self) -> bool {
        self.blocks_slice().iter().all(|block| *block == 0)
    }

    /// Returns `true` if the set is a subset of another, i.e. `other`
    /// contains at least all the values in `self`.
    pub fn is_subset(&self, other: &Self) -> bool {
        let self_slice = self.blocks_slice();
        let other_slice = other.blocks_slice();
        self_slice
            .iter()
            .zip(other_slice)
            .all(|(this, that)| *this & *that == *this)
    }

    /// Returns `true` if the set is a subset of another, i.e. `self`
    /// contains at least all the values in `other`.
    pub fn is_superset(&self, other: &Self) -> bool {
        other.is_subset(self)
    }

    /// Returns `true` if `self` has no elements in common with other. This is
    /// equivalent to checking for an empty intersection.
    pub fn is_disjoint(&self, other: &Self) -> bool {
        let self_slice = self.blocks_slice();
        let other_slice = other.blocks_slice();
        self_slice
            .iter()
            .zip(other_slice)
            .all(|(x, y)| *x & *y == 0)
    }

    /// Populates the union, i.e. all the values in `self`
    /// or `other`, without duplicates.
    /// ```rust
    /// # use std::error::Error;
    /// #
    /// # extern crate staticbitset;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use staticbitset::StaticBitSet;
    /// let mut a = StaticBitSet::<64>::default();
    /// let mut b = StaticBitSet::<64>::default();
    /// let mut c = StaticBitSet::<64>::default();
    /// a.insert(3);
    /// a.insert(2);
    /// b.insert(1);
    /// b.insert(4);
    /// a.union(&b, &mut c);
    /// assert_eq!(c.ones().collect::<Vec<usize>>(), [1,2,3,4]);
    /// #     Ok(())
    /// # }
    /// ```
    pub fn union(&self, other: &Self, union: &mut Self) {
        let self_slice = self.blocks_slice();
        let other_slice = other.blocks_slice();
        let union_slice = union.blocks_mut_slice();

        for (i, (x, y)) in self_slice.iter().zip(other_slice).enumerate() {
            union_slice[i] = *x | *y;
        }
    }
}

pub struct OnesIter<'a> {
    bit_index: usize,
    block_index: usize,
    blocks: &'a [BlockElt],
}
impl<'a> Iterator for OnesIter<'a> {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        for block in self.blocks[self.block_index..].iter() {
            let block_bit_index = self.block_index * BLOCK_SIZE_BITS;
            for j in self.bit_index..BLOCK_SIZE_BITS {
                self.bit_index = j;
                if (block >> j & 1) != 0 {
                    let absolute_bit_index = block_bit_index + j;
                    self.bit_index += 1;
                    return Some(absolute_bit_index);
                }
            }
            self.bit_index = 0;
            self.block_index += 1;
        }

        None
    }
}

#[cfg(test)]
#[macro_use]
extern crate std;
mod tests {
    #[cfg(test)]
    use crate::StaticBitSet;

    #[test]
    fn test_macro() {
        let foo = StaticBitSet::<1>::default();
        assert_eq!(foo.bit_blocks.len(), 1);

        let bar = StaticBitSet::<256>::default();
        assert_eq!(bar.bit_blocks.len(), 8);

        let baz = StaticBitSet::<257>::default();
        assert_eq!(baz.bit_blocks.len(), 9);

        use crate::BlockElt;
        use core::mem::size_of;
        type B64 = StaticBitSet<64>;
        assert_eq!(size_of::<B64>(), 2 * size_of::<BlockElt>());
    }

    #[test]
    fn test_count() {
        let mut x = StaticBitSet::<256>::default();
        assert_eq!(x.count_ones(), 0);
        assert_eq!(x.count_zeros(), 256);
        x.clear();
        assert_eq!(x.count_ones(), 0);
        x.insert(3);
        assert_eq!(x.count_ones(), 1);

        let mut y = x;
        assert_eq!(y.count_ones(), 1);
        y.clear();
        assert_eq!(y.count_ones(), 0);

        let mut z = x;
        assert_eq!(z.count_ones(), 1);
        assert_eq!(z.count_zeros(), 255);
        assert_eq!(x.count_zeros(), 255);
        z.set(3, false);
        assert_eq!(y.count_ones(), 0);

        let mut w = StaticBitSet::<256>::default();
        assert_eq!(w.count_ones(), 0);
        w.set(15, true);
        assert_eq!(w.count_ones(), 1);
        w.set(127, true);
        assert_eq!(w.count_ones(), 2);
        w.set(17, true);
        assert_eq!(w.count_ones(), 3);
        w.set(19, true);
        assert_eq!(w.count_ones(), 4);
        w.set(243, true);
        assert_eq!(w.count_ones(), 5);

        for i in 0..12 {
            w.insert(i);
        }
        assert_eq!(w.count_ones(), 17);

        w.clear();
        assert_eq!(w.count_ones(), 0);

        w = y;
        assert_eq!(w.count_ones(), 0);
        w = x;
        assert_eq!(w.count_ones(), 1);
    }

    #[test]
    fn test_contains() {
        let mut x = StaticBitSet::<256>::default();
        let (low, high) = (68, 98);
        for i in 0..256 {
            assert!(!x.contains(i));
        }

        for i in low..high {
            x.insert(i);
        }

        for i in low..high {
            assert!(x.contains(i));
        }
        for i in 0..low {
            assert!(!x.contains(i));
        }
        for i in high..256 {
            assert!(!x.contains(i));
        }
    }

    #[test]
    fn test_clear_set() {
        let mut x = StaticBitSet::<64>::default();
        assert!(!x.contains(3));
        x.clear();
        assert!(!x.contains(3));
        x.set(3, true);
        assert!(x.contains(3));
        assert!(x.contains(3));
        x.set(3, false);
        assert!(!x.contains(3));
        assert!(!x.contains(3));

        let mut y = x;
        assert!(!y.contains(3));
        y.clear();
        assert!(!x.contains(3));
        assert!(!y.contains(3));
    }

    #[test]
    fn test_logic() {
        let mut x = StaticBitSet::<256>::default();
        assert!(x.is_empty());
        x.insert(3);
        assert!(!x.is_empty());
        let z = x;
        assert_eq!(x, z);
        assert_eq!(x.contains(3), true);
        assert_eq!(z.contains(3), true);
        assert!(!x.is_disjoint(&z));
        assert!(!z.is_disjoint(&x));
        assert!(z.is_subset(&x));
        assert!(x.is_subset(&z));
        assert!(z.is_superset(&x));
        assert!(x.is_superset(&z));

        let mut y = StaticBitSet::<256>::default();
        for i in 0..12 {
            y.insert(i);
        }
        y.insert(99);

        assert!(!x.is_superset(&y));
        assert!(x.is_subset(&y));
        assert!(y.is_superset(&x));
        assert!(!y.is_subset(&x));

        y.remove(3);

        assert!(x.is_disjoint(&y));
        assert!(y.is_disjoint(&x));
        assert!(!x.is_subset(&y));
        assert!(!y.is_subset(&x));
        assert!(!x.is_superset(&y));
        assert!(!y.is_superset(&x));

        assert!(x.is_superset(&x));
        assert!(y.is_superset(&y));

        let mut w = StaticBitSet::<256>::default();
        x.union(&y, &mut w);
        assert_ne!(x, y);
        assert!(w.is_superset(&x));
        assert!(w.is_superset(&y));
        assert!(x.is_subset(&w));
        assert!(y.is_subset(&w));
        assert_ne!(w, x);
        assert_ne!(w, y);

        assert!(!z.is_empty());
        assert!(!w.is_empty());
        assert!(!x.is_empty());
        assert!(!y.is_empty());
    }

    #[test]
    #[should_panic]
    fn test_range_panics() {
        let mut x = StaticBitSet::<33>::default();
        x.insert(64);
    }

    #[test]
    fn test_range_valid() {
        let mut x = StaticBitSet::<33>::default();
        x.insert(0);
        x.insert(34);
        assert_eq!(x.count_ones(), 2);
    }

    #[test]
    fn test_ones() {
        let mut x = StaticBitSet::<256>::default();

        assert_eq!(x.ones().collect::<Vec<usize>>(), vec![]);

        for i in 60..70 {
            x.insert(i);
        }
        x.remove(65);

        use std::vec::Vec;
        let y: Vec<usize> = vec![60, 61, 62, 63, 64, 66, 67, 68, 69];
        assert_eq!(x.ones().collect::<Vec<usize>>(), y);
    }
}
